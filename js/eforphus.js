/**
 * @file
 * Contains the definition of the behaviour eforphus.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.eforphus = {
    attach: function (context, settings) {

      var fragment = '';

      $(document).once('cntr_enter').keydown(function(e) {

        if (e.ctrlKey && e.keyCode == 13 && Drupal.efOrphus.isInit()) {
          e.stopPropagation();
          e.preventDefault();
          Drupal.efOrphus.modal();
        }
      });

      /*
      $(document).keydown(function (e) {
        console.log(e.keyCode);

        fragment = getSelectedText();
        console.log(fragment);
      });
      */

      $('.use-eforphus', context).once('eforphusBehavior').each(function () {
        var entityId = $(this).data('eforphus-entity'),
            type = $(this).data('eforphus-type'),
            bundle = $(this).data('eforphus-bundle'),
            limit = $(this).data('eforphus-limit'),
            entityField = $(this).data('eforphus-field');

        $(this).on('mouseup', function (event) {
          fragment = getSelectedText();
          console.log(fragment);
          Drupal.efOrphus.setData(entityId, entityField, type, bundle, fragment, limit);

        })
      })
    }
  };


  Drupal.efOrphus = {
    entityId: null,
    entityField: null,
    type: null,
    fragment: '',
    bundle: '',
    limit: 0,
    modalOpened: false,

    modal: function () {
      var self = this;


      console.log(self.fragment.length);
      console.log(self.limit);

      if ((self.fragment.length > self.limit) && self.limit !== 0) {
        var args = {
          '@limit' : self.limit,
        };
        var modalDialog = Drupal.dialog('<div class="limit">' + Drupal.t('You have selected too large text block! <br>@limit character limit.', args) + '</div>', {
          title: Drupal.t('Report an error'),
          dialogClass: 'eforphus-modal',
          width: 400,
          height: 150,
          autoResize: true,
          close: function (event, ui) {
            Drupal.efOrphus.closeDialog();
          },
          buttons: [
            {
              text: Drupal.t('Close'),
              icons: {
                primary: 'ui-icon-close'
              },
              click: function () {
                $(this).dialog('close');
              }
            }
          ]
        });

        if (!self.modalOpened)  {
          modalDialog.showModal();
          self.modalOpened = true;
        }
      };

      if ((self.limit === 0) || (self.fragment.length <= self.limit)){

        var dialogContent = Drupal.t('<div class="fragment-message">Fragment of text: <span class="fragment">@fragment</span></div>', {
          '@fragment': self.fragment
        });

        modalDialog = Drupal.dialog(dialogContent, {
          title: Drupal.t('Report an error'),
          dialogClass: 'eforphus-modal',
          width: 400,
          height: 230,
          autoResize: true,
          close: function (event, ui) {
            Drupal.efOrphus.closeDialog();
          },
          buttons: [
            {
              text: Drupal.t('Report'),
              class: 'report-class',
              icons: {
                primary: 'ui-icon-alert'
              },
              click: function () {
                $(this).html('Thank you');
                Drupal.efOrphus.sendRequest();
                $(this).dialog('close');
              }
            },
            {
              text: Drupal.t('Close'),
              icons: {
                primary: 'ui-icon-close'
              },
              click: function () {
                $(this).dialog('close');
              }
            }
          ]
        });


        if (!self.modalOpened)  {
          modalDialog.showModal();
          self.modalOpened = true;
        }
      }



    },

    closeDialog: function () {
      var self = this;
      self.modalOpened = false;
    },

    reset: function() {},

    isInit: function () {
      var self = this;
      return (self.entityId != null &&
          self.entityField != null &&
          self.type != null);
    },
    setData: function (entityId, entityField, type, bundle, fragment, limit) {
      var self = this;

      self.entityId = entityId;
      self.entityField = entityField;
      self.fragment = fragment;
      self.type = type;
      self.bundle = bundle;
      self.limit = limit;
    },
    sendRequest: function () {
      var self = this;
      var jqxhr = $.ajax({
        url: '/eforphus/report',
        type: "POST",
        contentType: 'application/json',
        data: JSON.stringify({
          eid: self.entityId,
          field: self.entityField,
          fragment: self.fragment,
          type: self.type,
          bundle: self.bundle,
        }),
        success: function (data, textStatus, jQxhr) {
          console.log(data);
          console.log(textStatus);
        }
      })
    }
  };

})(jQuery, Drupal, drupalSettings);

var getSelectedText = function() {
  var text = '';
  if (window.getSelection) {
    text = window.getSelection().toString();
  } else if (document.selection) {
    text = document.selection.createRange().text;
  }
  return text;
};