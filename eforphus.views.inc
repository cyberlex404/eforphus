<?php
/**
 * Created by PhpStorm.
 * User: Lex
 * Date: 14.12.2018
 * Time: 17:14
 */

/**
 * Implements hook_views_data().
 */
function eforphus_views_data() {
  $data = [];

  $data['eforphus']['table']['group'] = t('Eforphus');
  $data['eforphus']['table']['provider'] = 'eforphus';
  //$data['eforphus']['table']['wizard_id'] = 'eforphus';

  $data['eforphus']['table']['base'] = [
    'field' => 'rid',
    'title' => t('Log eforphus'),
    'help' => t('Contains a list of log eforphus.'),
    'weight' => -10,
  ];

  $data['eforphus']['rid'] = [
    'title' => t('RID'),
    'help' => t('Unique eforphus ID.'),
    'field' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'numeric',
    ],
    'argument' => [
      'id' => 'numeric',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];

  $data['eforphus']['eid'] = [
    'title' => t('Entity ID'),
    'help' => t('Unique entity ID.'),
    'field' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'numeric',
    ],
    'argument' => [
      'id' => 'numeric',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];
  $data['eforphus']['entity_link'] = [
    'title' => t('Entity link'),
    'help' => t('Entity link to edit'),
    'field' => [
      'id' => 'eforphus_entity_link',
    ],
  ];
 // eforphus_entity_link
  // todo : Field link to entity

  $data['eforphus']['uid'] = [
    'title' => t('UID'),
    'help' => t('The user ID.'),
    'field' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'numeric',
    ],
    'argument' => [
      'id' => 'numeric',
    ],
    'relationship' => [
      'title' => t('User'),
      'help' => t('The user on which the log entry as written.'),
      'base' => 'users_field_data',
      'base field' => 'uid',
      'id' => 'standard',
    ],
  ];

  $data['eforphus']['fragment'] = [
    'title' => t('Fragment'),
    'help' => t('Fragment of text.'),
    'field' => [
      'id' => 'standard',
    ],
    'argument' => [
      'id' => 'string',
    ],
    'filter' => [
      'id' => 'string',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];

  $data['eforphus']['field'] = [
    'title' => t('Field'),
    'help' => t('Field of entity.'),
    'field' => [
      'id' => 'standard',
    ],
    'argument' => [
      'id' => 'string',
    ],
    'filter' => [
      'id' => 'string',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];
// eforphus_field_name

  $data['eforphus']['field_name'] = [
    'title' => t('Field name'),
    'help' => t('Field name of entity.'),
    'field' => [
      'id' => 'eforphus_field_name',
    ],
  ];
  $data['eforphus']['message'] = [
    'title' => t('Message'),
    'help' => t('User comment.'),
    'field' => [
      'id' => 'standard',
    ],
    'argument' => [
      'id' => 'string',
    ],
    'filter' => [
      'id' => 'string',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];

  $data['eforphus']['type'] = [
    'title' => t('Entity type'),
    'help' => t('Entity type.'),
    'field' => [
      'id' => 'standard',
    ],
    'argument' => [
      'id' => 'string',
    ],
    'filter' => [
      'id' => 'string',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];

  $data['eforphus']['bundle'] = [
    'title' => t('Bundle'),
    'help' => t('Entity bundle.'),
    'field' => [
      'id' => 'standard',
    ],
    'argument' => [
      'id' => 'string',
    ],
    'filter' => [
      'id' => 'string',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];

  $data['eforphus']['hostname'] = [
    'title' => t('Hostname'),
    'help' => t('Hostname of the user who triggered the event.'),
    'field' => [
      'id' => 'standard',
    ],
    'argument' => [
      'id' => 'string',
    ],
    'filter' => [
      'id' => 'string',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];

  $data['eforphus']['timestamp'] = [
    'title' => t('Timestamp'),
    'help' => t('Just a timestamp field.'),
    'field' => [
      'id' => 'date',
    ],
    'sort' => [
      'id' => 'date',
    ],
    'filter' => [
      'id' => 'date',
    ],
  ];

  return $data;
}