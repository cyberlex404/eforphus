<?php

namespace Drupal\eforphus;

/**
 * Interface OrphusReportsInterface.
 */
interface OrphusReportsInterface {

  /**
   * @param array $entry
   *
   * @throws \Exception
   */
  public function report(array $entry);

  /**
   * @param array $entry
   *
   * @return \stdClass[]|mixed
   */
  public function load(array $entry = []);

  /**
   * @param $rid
   *
   * @return \stdClass|null
   */
  public function loadById($rid);

  /**
   * @param string $type
   * @param string $bundle
   * @param string $field
   *
   * @return mixed
   */
  public function loadConfig($type, $bundle, $field);

  /**
   * @return mixed
   */
  public function defaultConfig();

}
