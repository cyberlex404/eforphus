<?php

namespace Drupal\eforphus\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use Drupal\eforphus\OrphusReportsInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ReportController.
 */
class ReportController extends ControllerBase {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\eforphus\OrphusReportsInterface definition.
   *
   * @var \Drupal\eforphus\OrphusReportsInterface
   */
  protected $eforphus;

  /**
   * Constructs a new ReportController object.
   */
  public function __construct(ConfigFactoryInterface $config_factory, OrphusReportsInterface $eforphus) {
    $this->configFactory = $config_factory;
    $this->eforphus = $eforphus;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('eforphus.reports')
    );
  }

  /**
   * Report callback.
   *
   * @param Request $request
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function report(Request $request) {

    $data = $request->getContent();
    $entry = Json::decode($data);

    \Drupal::logger('eforphus')->debug('d ' . Json::encode($entry));

    $entry += [
      'hostname' => $request->getClientIp(),
      'timestamp' => time(),
    ];

    try {
      $entry = $this->eforphus->report($entry);
    } catch (\Exception $e) {
    }

    $response = new JsonResponse(['data' => $entry]);

    return $response;
  }

}
