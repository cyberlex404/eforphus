<?php
/**
 * @file
 * Definition of Drupal\eforphus\Plugin\views\field\EntityLink
 */

namespace Drupal\eforphus\Plugin\views\field;

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Field handler Entity link.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("eforphus_entity_link")
 */
class EntityLink extends FieldPluginBase {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a LinkBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entityTypeManager;
  }


  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * @{inheritdoc}
   */
  public function query() {
    $this->ensureMyTable();
    $this->query->addField('eforphus', 'type', 'type');
    $this->query->addField('eforphus', 'eid', 'eid');
  }

  /**
   * @{inheritdoc}
   */
  public function render(ResultRow $values) {
    $entity = $this->entityTypeManager->getStorage($values->type)->load($values->eid);
    $link = $entity->toLink($this->t('Edit'), 'edit-form');
    return $link->toRenderable();
  }


}
