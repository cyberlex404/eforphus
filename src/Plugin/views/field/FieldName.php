<?php
/**
 * @file
 * Definition of Drupal\eforphus\Plugin\views\field\FieldName
 */

namespace Drupal\eforphus\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Field handler to field name.
 * @todo Use DI
 * @see \Drupal\views\Plugin\views\field\LinkBase
 * @ingroup views_field_handlers
 *
 * @ViewsField("eforphus_field_name")
 */
class FieldName extends FieldPluginBase {

  /**
   * @{inheritdoc}
   */
  public function query() {
    $this->ensureMyTable();
    $this->query->addField('eforphus', 'field', 'field');
    $this->query->addField('eforphus', 'bundle', 'bundle');
    $this->query->addField('eforphus', 'type', 'type');
  }

  /**
   * @{inheritdoc}
   */
  public function render(ResultRow $values) {
    $fieldDefinitions = \Drupal::service('entity_field.manager')
      ->getFieldDefinitions($values->type, $values->bundle);
    $fieldDefinition = $fieldDefinitions[$values->field];
    return $fieldDefinition->getLabel();
  }

}
