<?php

/**
 * @file
 * Contains Drupal\eforphus\Plugin\QueueWorker\ReportNotifyMailer.php
 */

namespace Drupal\eforphus\Plugin\QueueWorker;

/**
 * A Report notify on CRON run.
 *
 * @QueueWorker(
 *   id = "eforphus_report_notify",
 *   title = @Translation("FEO Report notify"),
 *   cron = {"time" = 20}
 * )
 */
class ReportNotifyMailer extends ReportNotifyBase {}