<?php

/**
 * @file
 * Contains Drupal\eforphus\Plugin\QueueWorker\ReportNotifyBase.php
 */

namespace Drupal\eforphus\Plugin\QueueWorker;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Utility\Token;
use Drupal\eforphus\OrphusReportsInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class ReportNotifyBase extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\eforphus\OrphusReportsInterface definition.
   *
   * @var \Drupal\eforphus\OrphusReportsInterface
   */
  protected $eforphus;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;
  /**
   * Creates a new ReportNotifyBase object.
   *
   * @param OrphusReportsInterface $eforphus
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The EntityTypeManagerInterface.
   * @param Token $token
   */
  public function __construct(OrphusReportsInterface $eforphus, EntityTypeManagerInterface $entity_type_manager, Token $token,  MailManagerInterface $mailManager) {
    $this->eforphus = $eforphus;
    $this->entityTypeManager = $entity_type_manager;
    $this->token = $token;
    $this->mailManager = $mailManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('eforphus.reports'),
      $container->get('entity_type.manager'),
      $container->get('token'),
      $container->get('plugin.manager.mail')
    );
  }

  public function processItem($data) {

    $report = $this->eforphus->load($data->rid);

    // TODO: Implement processItem() method.
  }


}