<?php

namespace Drupal\eforphus\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'eforphus.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('eforphus.settings');
    $form['custom_email'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use custom e-mail'),
      '#default_value' => $config->get('custom_email'),
    ];
    $form['email'] = [
      '#type' => 'email',
      '#title' => $this->t('E-mail'),
      '#default_value' => $config->get('email'),
      '#states' => [
        'invisible' => [
          ':input[name="custom_email"]' => ['checked' => FALSE],
        ],
        'required' => [
          ':input[name="custom_email"]' => ['checked' => TRUE],
        ]
      ],
    ];
    $form['max_length'] = [
      '#type' => 'number',
      '#title' => $this->t('Max length'),
      '#default_value' => $config->get('max_length'),
      '#description' => $this->t('Use 0 for unlimited.'),
      '#min' => 0,
      '#required' => TRUE,
    ];
    $form['host_limit'] = [
      '#type' => 'number',
      '#title' => $this->t('Daily limit'),
      '#default_value' => $config->get('host_limit'),
      '#description' => $this->t('Use 0 for unlimited.'),
      '#min' => 0,
      '#required' => TRUE,
    ];
    $form['message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message in pop-up'),
      '#default_value' => $config->get('message'),
      '#description' => $this->t('Do not use HTML markup.'),
      '#required' => TRUE,
    ];

    $form['template'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Notification template'),
      '#default_value' => $config->get('template'),
      '#description' => $this->t('Notification template. (Token support)'),
      '#required' => TRUE,
    ];

    $form['token_tree'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => ['eforphus'],
      '#click_insert' => TRUE,
      '#show_restricted' => TRUE,
      '#recursion_limit' => 3,
    ];

    /**
     * @var $service \Drupal\eforphus\OrphusReportsInterface
     */
    $service = \Drupal::service('eforphus.reports');

    $settings = $service->defaultConfig();
    $settings = $service->loadConfig('node', 'article', 'body');
    $items = $service->load();
    dpm($settings);
    $item = reset($items);
    dpm($item, 'entry');


    /**
     * @var $token \Drupal\Core\Utility\Token
     */
    $token = \Drupal::service('token');

    $replace = $token->replace('Text: <em>[eforphus:fragment] 
[eforphus:field-id]
[eforphus:field-name]
[eforphus:timestamp]</em> <span>[eforphus:entity-link]</span><em>[eforphus:user:mail]</em>', [
      'eforphus' => $item,
    ], ['clear' => TRUE]);

    dpm($replace);

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('eforphus.settings')
      ->set('custom_email', $form_state->getValue('custom_email'))
      ->set('email', $form_state->getValue('email'))
      ->set('max_length', $form_state->getValue('max_length'))
      ->set('host_limit', $form_state->getValue('host_limit'))
      ->set('message', $form_state->getValue('message'))
      ->set('template', $form_state->getValue('template'))
      ->save();
  }

}
