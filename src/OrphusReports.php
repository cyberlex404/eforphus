<?php

namespace Drupal\eforphus;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Database\Driver\mysql\Connection;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Psr\Log\LoggerInterface;

/**
 * Class OrphusReports.
 */
class OrphusReports implements OrphusReportsInterface {

  use StringTranslationTrait;

  const TABLE = 'eforphus';
  /**
   * Drupal\Core\Database\Driver\mysql\Connection definition.
   *
   * @var \Drupal\Core\Database\Driver\mysql\Connection
   */
  protected $database;
  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Psr\Log\LoggerInterface definition.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $queue;

  /**
   * @var array
   */
  protected $defaultConfig;
  /**
   * Constructs a new OrphusReports object.
   */
  public function __construct(Connection $database,
                              ConfigFactoryInterface $config_factory,
                              EntityTypeManagerInterface $entityTypeManager,
                              AccountProxyInterface $currentUser,
                              LoggerInterface $logger,
                              QueueFactory $queueFactory) {
    $this->database = $database;
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entityTypeManager;
    $this->currentUser = $currentUser;
    $this->logger = $logger;
    $this->queue = $queueFactory->get('eforphus_report_notify');
  }


  /**
   * {@inheritdoc}
   */
  public function report(array $entry) {

    if(!isset($entry['type']) || !isset($entry['eid']) ||
      !isset($entry['field']) || !isset($entry['fragment']) || !isset($entry['bundle'])){
      throw new \Exception('Incorrect data');
    }

    $entry['uid'] = $this->currentUser->id();



    // todo: validate $entry

    $return_value = NULL;
    try {
      $return_value = $this->database->insert(self::TABLE)
        ->fields($entry)
        ->execute();
    }
    catch (\Exception $e) {
      $this->logger->debug($this->t('db_insert failed. Message = %message', [
        '%message' => $e->getMessage(),
      ]));
    }

    if ($return_value) {
      $item = new \stdClass();
      $item->rid = $return_value;
      $this->queue->createItem($item);
    }
    return $return_value;
  }

  /**
   * {@inheritdoc}
   */
  public function load(array $entry = []) {
    $select = $this->database
      ->select(self::TABLE)
      ->fields(self::TABLE);

    foreach ($entry as $field => $value) {
      $select->condition($field, $value);
    }
    return $select->execute()->fetchAll();
  }

  /**
   * {@inheritdoc}
   */
  public function loadById($rid) {
    return reset($this->load(['rid' => $rid]));
  }

  public function loadConfig($type, $bundle, $field) {

    $renderer = \Drupal::
    $view_builder = $this->entityTypeManager->getViewBuilder($type);

    $fieldDefinitions = \Drupal::service('entity_field.manager')
      ->getFieldDefinitions($type, $bundle);
    $fieldDefinition = $fieldDefinitions[$field];
    dpm($fieldDefinitions);

  }


  public function defaultConfig() {
    if (!isset($this->defaultConfig)) {
      $config = $this->configFactory->get('eforphus.settings');
      $siteConfig = $this->configFactory->get('system.site');

      $settings['email'] = ($config->get('custom_email')) ? $config->get('email') : $siteConfig->get('mail');
      $settings['template'] = $config->get('template');
      $settings['message'] = $config->get('message');
      $settings['host_limit'] = $config->get('host_limit');
      $settings['max_length'] = $config->get('max_length');
      $this->defaultConfig = $settings;
    }
    return $this->defaultConfig;
  }



}
